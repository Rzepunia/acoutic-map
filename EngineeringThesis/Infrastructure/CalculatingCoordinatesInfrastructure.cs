﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Infrastructure
{
    public class CalculatingCoordinatesInfrastructure :ICalculatingCoordinatesInfrastructure
    {
        public float CalculateLatitude(string latitude)
        {
            var deg = float.Parse(latitude.Substring(0, 2));
            var min = float.Parse(latitude.Substring(2,6), CultureInfo.InvariantCulture);
            var minToDeg = min/60;
            var sum = deg + minToDeg;
            if (latitude.Substring(10) == "S")
            {
                sum *= (-1);
            }
            return sum;
        }

        public float CalculateLongitude(string longitude)
        {
            var deg = float.Parse(longitude.Substring(0, 3));
            var min = float.Parse(longitude.Substring(3, 6), CultureInfo.InvariantCulture);
            var minToDeg = min / 60;
            var sum = deg + minToDeg;
            if (longitude.Substring(11) == "W")
            {
                sum *= (-1);
            }
            return sum;
        }
    }
}