﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineeringThesis.Infrastructure
{
    public interface ICalculatingCoordinatesInfrastructure
    {
        float CalculateLatitude(string latitude);
        float CalculateLongitude(string longitude);
    }
}
