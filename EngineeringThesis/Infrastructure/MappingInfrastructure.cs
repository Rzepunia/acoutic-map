﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;
using EngineeringThesis.Services;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EngineeringThesis.Infrastructure
{
    public class MappingInfrastructure : IMappingInfrastructure
    {
        private IMapper _mapper;

        public MappingInfrastructure(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Group MapGroupDtoToGroup(CreateGroupDto group)
        {
            var groupEntity = _mapper.Map<CreateGroupDto, Group>(group);
            return groupEntity;
        }

        public GroupViewModel MapGroupAndRolesToGroupViewModel(Group group)
        {
            var model = _mapper.Map<Group, GroupViewModel>(group);
            model.Roles = group.Roles.Select(a => a.Role.Name).ToList();
            return model;
        }

        public GroupDetailsViewModel MapGroupToGroupDetailsViewModel(Group group)
        {
            var model = _mapper.Map<Group, GroupDetailsViewModel>(group);
            model.Users = group.Users.Select(a => a.User.UserName).ToList();
            model.Roles = group.Roles.Select(a => a.Role.Name).ToList();
            return model;
        }

        public CreateGroupViewModel MapRolesToCreateGroupViewModel(List<IdentityRole> roles)
        {
            var model = new CreateGroupViewModel();
            foreach (var role in roles)
            {
                var listItem = new SelectListItem()
                {
                    Text = role.Name,
                    Value = role.Id,
                };
                model.Roles.Add(listItem);
            }
            return model;
        }

        public EditGroupViewModel MapGroupAndRolesToEditGroupViewModel(Group group, IEnumerable<IdentityRole> SelectedRoles, List<IdentityRole> allRoles)
        {
            var model = new EditGroupViewModel()
            {
                Id = group.Id,
                GroupName = group.GroupName,
                GroupDescription = group.GroupDescription,
            };
            foreach (var role in allRoles)
            {
                var listItem = new SelectListItem()
                {
                    Text = role.Name,
                    Value = role.Id,
                    Selected = SelectedRoles.Any(a => a.Id == role.Id)
                };
                model.Roles.Add(listItem);
            }
            return model;
        }

        public Group MapEditGroupDtoToGroup(EditGroupDto group)
        {
            var groupEntity = _mapper.Map<EditGroupDto, Group>(group);
            return groupEntity;
        }

        public AssignUserToGroupViewModel MapUsersAndGroupIdToAssignUserToGroupViewModel(GroupIdAndNameDto group, IEnumerable<ApplicationUser> actualUsers, IEnumerable<ApplicationUser> allUsers)
        {
            var model = _mapper.Map<GroupIdAndNameDto, AssignUserToGroupViewModel>(group);
            foreach (var user in allUsers)
            {
                var listItem = new SelectListItem()
                {
                    Text = user.UserName,
                    Value = user.Id,
                    Selected = actualUsers.Any(a => a.Id == user.Id)
                };
                model.Users.Add(listItem);
            }
            return model;
        }

        public ManageUsersViewModel MapUserAndGroupsToManageUsersViewModel(ApplicationUser user, List<string> groups)
        {
            var model = _mapper.Map<ApplicationUser, ManageUsersViewModel>(user);
            model.GroupsList = groups;
            return model;
        }

        public UserDetailsViewModel MapUserAndGroupsToDetailsViewModel(ApplicationUser user, List<string> groupsList)
        {
            var model = _mapper.Map<ApplicationUser, UserDetailsViewModel>(user);
            model.GroupsList = groupsList;
            return model;
        }

        public RegisterUserByAdminViewModel MapAllGroupsToRegisterUserByAdminViewModel(IEnumerable<Group> allGroups)
        {
            var model = new RegisterUserByAdminViewModel();
            foreach (var group in allGroups)
            {
                var listItem = new SelectListItem()
                {
                    Text = group.GroupName,
                    Value = group.Id
                };
                model.GroupsList.Add(listItem);
            }
            return model;
        }

        public ApplicationUser MapRegisterUserByAdminDtoToApplicationUser(RegisterUserByAdminDto model)
        {
            var user = _mapper.Map<RegisterUserByAdminDto, ApplicationUser>(model);
            return user;
        }

        public EditUserViewModel MapUserAndSelectedGroupsToEditUserViewModel(ApplicationUser user, IEnumerable<Group> allGroups,
            List<Group> userGroups)
        {
            var model = _mapper.Map<ApplicationUser, EditUserViewModel>(user);
            foreach (var group in allGroups)
            {
                var listItem = new SelectListItem()
                {
                    Text = group.GroupName,
                    Value = group.Id,
                    Selected = userGroups.Any(a => a.Id == group.Id)
                };
                model.GroupsList.Add(listItem);
            }
            return model;
        }

        public List<ManagePointsViewModel> MapListOfPointsToListOfManagePointsViewModel(List<Point> points)
        {
            var list = _mapper.Map<List<Point>, List<ManagePointsViewModel>>(points);
            return list;
        }

        public PointDetailsViewModel MapPointToPointDetailsViewModel(Point point)
        {
            var model = _mapper.Map<Point, PointDetailsViewModel>(point);
            return model;
        }

        public CreatePointViewModel MapCreatePointDtoToCreatePointViewModel(CreatePointDto model)
        {
            var viewModel = _mapper.Map<CreatePointDto, CreatePointViewModel>(model);
            return viewModel;
        }

        public Point MapCreatePointDtoToPointEntity(CreatePointDto model)
        {
            var entity = _mapper.Map<CreatePointDto, Point>(model);
            return entity;
        }

        public EditPointViewModel MapPointToEditPointViewModel(Point entity)
        {
            var model = _mapper.Map<Point, EditPointViewModel>(entity);
            return model;
        }

        public EditPointViewModel MapEditPointDtoToCreatePointViewModel(EditPointDto model)
        {
            var viewModel = _mapper.Map<EditPointDto, EditPointViewModel>(model);
            return viewModel;
        }

        public Point MapEditPointDtoToPointEntity(EditPointDto model)
        {
            var entity = _mapper.Map<EditPointDto, Point>(model);
            return entity;
        }

        public List<ManageAreasViewModel> MapListOfAreasToListOfManageAreasViewModel(List<Area> areas)
        {
            var viewModel = _mapper.Map<List<Area>, List<ManageAreasViewModel>>(areas);
            return viewModel;
        }

        public Area MapCreateAreaDtoToAreaEntity(CreateAreaDto model)
        {
            var entity = _mapper.Map<CreateAreaDto, Area>(model);
            return entity;
        }

        public CreateAreaViewModel MapCreateAreaDtoToCreateAreaViewModel(CreateAreaDto model)
        {
            var viewModel = _mapper.Map<CreateAreaDto, CreateAreaViewModel>(model);
            return viewModel;
        }

        public EditAreaViewModel MapAreaToEditAreaViewModel(Area entity)
        {
            var model = _mapper.Map<Area, EditAreaViewModel>(entity);
            return model;
        }

        public Area MapEditAreaDtoToAreaEntity(EditAreaDto model)
        {
            var entity = _mapper.Map<EditAreaDto, Area>(model);
            return entity;
        }

        public EditAreaViewModel MapEditAreaDtoToCreateAreaViewModel(EditAreaDto model)
        {
            var viewModel = _mapper.Map<EditAreaDto, EditAreaViewModel>(model);
            return viewModel;
        }

        public AssignPointsToAreaViewModel MapAreaToAssignPointsToAreaViewModel(Area area, List<Point> allPoints)
        {
            var viewModel = new AssignPointsToAreaViewModel()
            {
                Id = area.Id,
                Name = area.Name
            };
            if (allPoints.Count != 0)
            {
                foreach (var item in allPoints)
                {
                    var listItem = new SelectListItem()
                    {
                        Text = item.Name,
                        Value = item.Id,
                        Selected = area.Points?.Any(a => a.Id == item.Id) ?? false
                    };
                    viewModel.Points.Add(listItem);
                }
            }
            return viewModel;
        }

        public List<MeasurementViewModel> MapListOfMeasurementsToListOfMeasurementsViewModel(List<Measurement> listOfEntities)
        {
            var list = _mapper.Map<List<Measurement>, List<MeasurementViewModel>>(listOfEntities);
            return list;
        }

        public ChartDataViewModel MapListOfMeasurementsToChartDataViewModel(List<Measurement> measurements)
        {
            var model = new ChartDataViewModel();
            var sorted = measurements.OrderBy(d => d.DateTimeStamp.Year)
                .ThenBy(d => d.DateTimeStamp.Month).ThenBy(d => d.DateTimeStamp.Day);
            foreach (var item in sorted)
            {
                model.Labels.Add(item.DateTimeStamp.ToString("dd-MM yyyy"));
                model.Values.Add(item.Value);
            }
            return model;
        }

        public List<MapMarkersViewModel> MapListOfMeasurementsToListOfMapMarkersViewModel(List<Measurement> data)
        {
            var list = _mapper.Map<List<Measurement>, List<MapMarkersViewModel>>(data);
            return list;
        }

        public List<SelectListItem> MapListOfMeasurementsToListOfSelectListItem(List<Measurement> data)
        {
            var list = _mapper.Map<List<Measurement>, List<SelectListItem>>(data);
            return list;
        }

        public AreaDetailsViewModel MapAreaToAreaDetailsViewModel(Area area)
        {
            var model = _mapper.Map<Area, AreaDetailsViewModel>(area);
            return model;
        }

        public List<SelectListItem> MapListOfAreasToListOfSelectListItem(List<Area> areas)
        {
            var list = _mapper.Map<List<Area>, List<SelectListItem>>(areas);
            return list;
        }

        public List<SelectListItem> MapListOfPointsToListOfSelectListItem(List<Point> points)
        {
            var list = _mapper.Map<List<Point>, List<SelectListItem>>(points);
            return list;
        }

        public Measurement MapPointIdTimeAndMsrtValueToMeasurementEntity(string pointId, int measruementValue, DateTime parsedTime)
        {
            var entity = new Measurement()
            {
                Id = Guid.NewGuid().ToString(),
                DateTime = parsedTime,
                PointId = pointId,
                Value = measruementValue,
                DateTimeStamp = DateTime.Now
            };
            return entity;
        }

        public RegisterUserByAdminViewModel MapAllGroupsAndRegisterUserByAdminDtoToRegisterUserByAdminViewModel(IEnumerable<Group> allGroups,
            RegisterUserByAdminDto model)
        {
            var viewModel = _mapper.Map<RegisterUserByAdminDto, RegisterUserByAdminViewModel>(model);
            foreach (var group in allGroups)
            {
                var listItem = new SelectListItem()
                {
                    Text = group.GroupName,
                    Value = group.Id,
                    Selected = model.SelectedGroups?.Any(a => a == group.Id) ?? true
                };
                viewModel.GroupsList.Add(listItem);
            }
            return viewModel;
        }

        public EditUserViewModel MapUserSelectedGroupsAndInvalidModelToEditUserViewModel(ApplicationUser user, IEnumerable<Group> allGroups,
            List<Group> userGroups, EditUserDto userDto)
        {
            var model = _mapper.Map<ApplicationUser, EditUserViewModel>(user);
            model.Username = userDto.Username;
            model.FullName = userDto.FullName;
            foreach (var group in allGroups)
            {
                var listItem = new SelectListItem()
                {
                    Text = group.GroupName,
                    Value = group.Id,
                    Selected = userDto.SelectedGroups?.Any(a => a == group.Id) ?? true
                };
                model.GroupsList.Add(listItem);
            }
            return model;
        }
    }
}