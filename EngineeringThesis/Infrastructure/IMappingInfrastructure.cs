﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;
using EngineeringThesis.Services;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace EngineeringThesis.Infrastructure
{
    public interface IMappingInfrastructure
    {
        Group MapGroupDtoToGroup(CreateGroupDto group);
        GroupViewModel MapGroupAndRolesToGroupViewModel(Group group);
        GroupDetailsViewModel MapGroupToGroupDetailsViewModel(Group group);
        CreateGroupViewModel MapRolesToCreateGroupViewModel(List<IdentityRole> roles);
        EditGroupViewModel MapGroupAndRolesToEditGroupViewModel(Group group, IEnumerable<IdentityRole> SelectedRoles, List<IdentityRole> allRoles);
        Group MapEditGroupDtoToGroup(EditGroupDto group);
        AssignUserToGroupViewModel MapUsersAndGroupIdToAssignUserToGroupViewModel(GroupIdAndNameDto group, IEnumerable<ApplicationUser> actualUsers, IEnumerable<ApplicationUser> allUsers);
        ManageUsersViewModel MapUserAndGroupsToManageUsersViewModel(ApplicationUser user, List<string> groups);
        UserDetailsViewModel MapUserAndGroupsToDetailsViewModel(ApplicationUser user, List<string> groupsList);
        RegisterUserByAdminViewModel MapAllGroupsToRegisterUserByAdminViewModel(IEnumerable<Group> allGroups);
        ApplicationUser MapRegisterUserByAdminDtoToApplicationUser(RegisterUserByAdminDto model);
        EditUserViewModel MapUserAndSelectedGroupsToEditUserViewModel(ApplicationUser user, IEnumerable<Group> allGroups, List<Group> userGroups);
        List<ManagePointsViewModel> MapListOfPointsToListOfManagePointsViewModel(List<Point> points);
        PointDetailsViewModel MapPointToPointDetailsViewModel(Point point);
        CreatePointViewModel MapCreatePointDtoToCreatePointViewModel(CreatePointDto model);
        Point MapCreatePointDtoToPointEntity(CreatePointDto model);
        EditPointViewModel MapPointToEditPointViewModel(Point entity);
        EditPointViewModel MapEditPointDtoToCreatePointViewModel(EditPointDto model);
        Point MapEditPointDtoToPointEntity(EditPointDto model);
        List<ManageAreasViewModel> MapListOfAreasToListOfManageAreasViewModel(List<Area> areas);
        Area MapCreateAreaDtoToAreaEntity(CreateAreaDto model);
        CreateAreaViewModel MapCreateAreaDtoToCreateAreaViewModel(CreateAreaDto model);
        EditAreaViewModel MapAreaToEditAreaViewModel(Area entity);
        Area MapEditAreaDtoToAreaEntity(EditAreaDto model);
        EditAreaViewModel MapEditAreaDtoToCreateAreaViewModel(EditAreaDto model);
        AssignPointsToAreaViewModel MapAreaToAssignPointsToAreaViewModel(Area area, List<Point> allPoints);
        List<MeasurementViewModel> MapListOfMeasurementsToListOfMeasurementsViewModel(List<Measurement> listOfEntities);
        ChartDataViewModel MapListOfMeasurementsToChartDataViewModel(List<Measurement> measurements);
        List<MapMarkersViewModel> MapListOfMeasurementsToListOfMapMarkersViewModel(List<Measurement> data);
        List<SelectListItem> MapListOfMeasurementsToListOfSelectListItem(List<Measurement> data);
        AreaDetailsViewModel MapAreaToAreaDetailsViewModel(Area area);
        List<SelectListItem> MapListOfAreasToListOfSelectListItem(List<Area> areas);
        List<SelectListItem> MapListOfPointsToListOfSelectListItem(List<Point> points);
        Measurement MapPointIdTimeAndMsrtValueToMeasurementEntity(string pointId, int measruementValue, DateTime parsedTime);
        RegisterUserByAdminViewModel MapAllGroupsAndRegisterUserByAdminDtoToRegisterUserByAdminViewModel(IEnumerable<Group> allGroups, RegisterUserByAdminDto model);
        EditUserViewModel MapUserSelectedGroupsAndInvalidModelToEditUserViewModel(ApplicationUser user, IEnumerable<Group> allGroups, List<Group> userGroups, EditUserDto model);
    }
}
