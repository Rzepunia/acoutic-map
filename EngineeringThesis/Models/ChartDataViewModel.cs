﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Models
{
    public class ChartDataViewModel
    {
        public ChartDataViewModel()
        {
            Labels = new List<string>();
            Values = new List<int>();
        }
        public List<string> Labels { get; set; }
        public List<int> Values { get; set; }

    }
}