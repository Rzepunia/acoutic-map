﻿using System.Collections.Generic;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Controllers
{
    public class ManagePointsViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public virtual ICollection<AreaViewModel> Areas { get; set; }
    }
}