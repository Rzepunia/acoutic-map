﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EngineeringThesis.Services
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {
            GroupsList = new List<SelectListItem>();
        }

        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Username")]
        [MinLength(5, ErrorMessage = "Min length of the Username is 5 characters")]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public ICollection<SelectListItem> GroupsList { get; set; }
    }
}