﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EngineeringThesis.Models
{
    public class GroupViewModel
    {
        public string Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public List<string> Roles { get; set; }
    }
}