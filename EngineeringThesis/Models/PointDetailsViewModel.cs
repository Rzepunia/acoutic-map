﻿using System.Collections.Generic;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Controllers
{
    public class PointDetailsViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Localization { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
    }
}