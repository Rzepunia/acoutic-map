﻿using System.Collections.Generic;

namespace EngineeringThesis.Services
{
    public class ManageUsersViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public List<string> GroupsList { get; set; }
    }
}