﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace EngineeringThesis.Services
{
    public class AssignUserToGroupViewModel
    {
        public AssignUserToGroupViewModel()
        {
            Users = new List<SelectListItem>();
        }
        public string Id { get; set; }
        public string GroupName { get; set; }
        public List<SelectListItem> Users { get; set; }
    }
}