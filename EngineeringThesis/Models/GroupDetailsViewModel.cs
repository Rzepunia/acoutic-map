﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Models
{
    public class GroupDetailsViewModel
    {
        public GroupDetailsViewModel()
        {
            Roles = new List<string>();
            Users = new List<string>();
        }

        public string Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public List<string> Roles { get; set; }
        public List<string> Users { get; set; }
    }
}