using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EngineeringThesis.Services
{
    public class CreateGroupViewModel
    {
        public CreateGroupViewModel()
        {
            Roles = new List<SelectListItem>();
        }
        [Required]
        [StringLength(30, ErrorMessage = "Group name must be min 5 characters, max 30", MinimumLength = 5)]
        public string GroupName { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "Group description must be at least 5 characters long")]
        public string GroupDescription { get; set; }
        public List<SelectListItem> Roles { get; set; }
    }
}