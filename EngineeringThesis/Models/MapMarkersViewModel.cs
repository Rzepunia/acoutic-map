﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Models
{
    public class MapMarkersViewModel
    {
        public string Name { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int Value { get; set; }
    }
}