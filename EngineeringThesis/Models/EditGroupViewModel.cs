﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EngineeringThesis.Models
{
    public class EditGroupViewModel
    {
        public EditGroupViewModel()
        {
            Roles = new List<SelectListItem>();
        }

        public string Id { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Group name must be min 5 characters, max 30", MinimumLength = 5)]
        public string GroupName { get; set; }
        [Required]
        [Display(Name = "Group description:")]
        public string GroupDescription { get; set; }

        public List<SelectListItem> Roles { get; set; }
    }
}