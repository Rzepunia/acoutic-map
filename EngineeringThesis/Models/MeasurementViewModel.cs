﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Models
{
    public class MeasurementViewModel
    {
        public string PointName { get; set; }
        public string DateTime { get; set; }
        public float Value { get; set; }
        public string DateTimeStamp { get; set; }
    }
}