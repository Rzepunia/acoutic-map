﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace EngineeringThesis.Controllers
{
    public class AssignPointsToAreaViewModel
    {
        public AssignPointsToAreaViewModel()
        {
            Points = new List<SelectListItem>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SelectListItem> Points { get; set; }
    }
}