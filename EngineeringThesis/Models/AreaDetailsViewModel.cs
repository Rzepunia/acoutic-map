﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Models
{
    public class AreaDetailsViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Point> Points { get; set; }
    }
}