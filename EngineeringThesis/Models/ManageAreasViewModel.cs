﻿using System.Collections.Generic;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Controllers
{
    public class ManageAreasViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<PointViewModel> Points { get; set; }
    }
}