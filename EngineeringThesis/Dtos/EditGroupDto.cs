using System.ComponentModel.DataAnnotations;

namespace EngineeringThesis.Controllers
{
    public class EditGroupDto
    {
        public string Id { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Group name must be min 5 characters, max 30", MinimumLength = 5)]
        public string GroupName { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "Group description must be at least 5 characters long")]
        public string GroupDescription { get; set; }
        public string[] SelectedRoles { get; set; }
    }
}