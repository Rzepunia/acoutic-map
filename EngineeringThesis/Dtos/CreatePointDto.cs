﻿using System.ComponentModel.DataAnnotations;

namespace EngineeringThesis.Controllers
{
    public class CreatePointDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Localization { get; set; }
        [RegularExpression(@"[0-9]*.[0-9]*")]
        public string Longitude { get; set; }
        [RegularExpression(@"[0-9]*.[0-9]*")]
        public string Latitude { get; set; }
    }
}