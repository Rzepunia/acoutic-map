﻿using System.ComponentModel.DataAnnotations;

namespace EngineeringThesis.Controllers
{
    public class CreateAreaDto
    {
        [Required]
        [StringLength(30, ErrorMessage = "Area name must be min 5 characters, max 30", MinimumLength = 5)]
        public string Name { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "Area description must be at least 5 characters long")]
        public string Description { get; set; }
    }
}