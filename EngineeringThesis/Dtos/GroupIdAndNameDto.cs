﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Dtos
{
    public class GroupIdAndNameDto
    {
        public string Id { get; set; }
        public string GroupName { get; set; }
    }
}