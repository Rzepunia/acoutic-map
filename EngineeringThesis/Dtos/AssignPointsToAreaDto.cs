﻿namespace EngineeringThesis.Controllers
{
    public class AssignPointsToAreaDto
    {
        public string Id { get; set; }
        public string[] SelectedPoints { get; set; }
    }
}