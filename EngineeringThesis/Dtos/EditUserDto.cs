﻿using System.ComponentModel.DataAnnotations;

namespace EngineeringThesis.Controllers
{
    public class EditUserDto
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Username")]
        [MinLength(5, ErrorMessage = "Min length of the Username is 5 characters")]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string[] SelectedGroups { get; set; }
    }
}