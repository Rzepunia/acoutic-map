﻿using System.ComponentModel.DataAnnotations;

namespace EngineeringThesis.Controllers
{
    public class AssignUserToGroupDto
    {
        public string Id { get; set; }
        public string[] SelectedUsers { get; set; }
    }
}