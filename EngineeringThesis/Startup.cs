﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EngineeringThesis.Startup))]
namespace EngineeringThesis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
