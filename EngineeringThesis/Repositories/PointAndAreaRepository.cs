﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using EngineeringThesis.Controllers;
using EngineeringThesis.Entities;
using EngineeringThesis.Services;

namespace EngineeringThesis.Repositories
{
    public class PointAndAreaRepository : IPointAndAreaRepository
    {
        private readonly ApplicationDbContext _context;

        public PointAndAreaRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Point> GetAllPoints()
        {
            var list = _context.Points.ToList();
            return list;
        }

        public void DeletePoint(string id)
        {
            var point = FindPointById(id);
            var msrts = _context.Measurements.Where(a => a.PointId == point.Id);
            _context.Measurements.RemoveRange(msrts);
            _context.Points.Remove(point);
            _context.SaveChanges();
        }

        public Point FindPointById(string id)
        {
            var point = _context.Points.Single(a => a.Id == id);
            return point;
        }

        public void AddNewPoint(Point entity)
        {
            _context.Points.Add(entity);
            _context.SaveChanges();
        }

        public Point GetPointById(string pointId)
        {
            var model = _context.Points.Single(a => a.Id == pointId);
            return model;
        }

        public void UpdatePoint(Point entity)
        {
            _context.Set<Point>().AddOrUpdate(entity);
            _context.SaveChanges();
        }

        public List<Area> GetAllAreas()
        {
            var list = _context.Areas.ToList();
            return list;
        }

        public void DeleteArea(string id)
        {
            var model = FindAreaById(id);
            _context.Areas.Remove(model);
            _context.SaveChanges();
        }

        public Area FindAreaById(string id)
        {
            var model = _context.Areas.Single(a => a.Id == id);
            return model;
        }

        public void AddNewArea(Area entity)
        {
            _context.Areas.Add(entity);
            _context.SaveChanges();
        }

        public Area GetAreaById(string areaId)
        {
            var model = _context.Areas.Single(a => a.Id == areaId);
            return model;
        }

        public void UpdateArea(Area entity)
        {
            _context.Set<Area>().AddOrUpdate(entity);
            _context.SaveChanges();
        }

        public void AssignPointsToArea(string areaId, string[] pointsToAssign)
        {
            var area = FindAreaById(areaId);
            area.Points.Clear();
            var listOfPoints = _context.Points.Where(a => pointsToAssign.Any(x => pointsToAssign.Contains(a.Id))).ToList();
            foreach (var item in listOfPoints)
            {
                area.Points.Add(item);
            }
            _context.SaveChanges();
        }
    }
}