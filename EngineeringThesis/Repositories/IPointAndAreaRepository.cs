﻿using System.Collections.Generic;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Services
{
    public interface IPointAndAreaRepository
    {
        List<Point> GetAllPoints();
        void DeletePoint(string id);
        Point FindPointById(string id);
        void AddNewPoint(Point entity);
        Point GetPointById(string pointId);
        void UpdatePoint(Point entity);
        List<Area> GetAllAreas();
        void DeleteArea(string id);
        Area FindAreaById(string id);
        void AddNewArea(Area entity);
        Area GetAreaById(string areaId);
        void UpdateArea(Area entity);
        void AssignPointsToArea(string areaId, string[] pointsToAssign);
    }
}