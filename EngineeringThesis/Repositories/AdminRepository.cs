﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EngineeringThesis.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly ApplicationDbContext _context;

        public AdminRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string CreateGroupAndReturnId(Group group)
        {
            _context.Groups.Add(group);
            _context.SaveChanges();
            return group.Id;
        }

        public void DeleteGroup(Group group)
        {
            _context.Groups.Remove(group);
            _context.SaveChanges();
        }

        public Group FindGroupById(string id)
        {
            var group = _context.Groups.FirstOrDefault(a => a.Id == id);
            return group;
        }

        public void UpdateGroup(Group group)
        {
            _context.Set<Group>().AddOrUpdate(group);
            _context.SaveChanges();
        }

        public void ClearGroupRoles(string groupId)
        {
            var group = FindGroupById(groupId);
            group.Roles.Clear();
            _context.SaveChanges();
        }

        public void AddRoleToGroup(string groupId, string role)
        {
            _context.GroupRoles.Add(new GroupRole()
            {
                GroupId = groupId,
                RoleId = role
            });
            _context.SaveChanges();
        }

        public void ResetRolesForGroupUsers(string groupId)
        {
            var group = FindGroupById(groupId);
            foreach (var user in group.Users)
            {
                RefreshUserGroupsRoles(user.UserId);
            }
        }

        public void ClearGroupsForUser(string userId, List<string> groupIds)
        {
            var groups = GetUserGroups(userId);
            foreach (var group in groups)
            {
                group.Users.Remove(group.Users.FirstOrDefault(a => a.UserId == userId));
            }
            _context.SaveChanges();
        }

        public List<Group> GetUserGroups(string userId)
        {
            var list =
                _context.Groups.Where(a => a.Users
                .Any(b => b.UserId == userId)).ToList();
            return list;
        }

        public List<string> GetUserGroupsIds(string userId)
        {
            var list =
               _context.Groups.Where(a => a.Users
               .Any(b => b.UserId == userId)).Select(c => c.Id).ToList();
            return list;
        }

        public void AddUserToGroups(string userId, string[] groupsIds)
        {
            if (groupsIds.Length != null)
            {
                foreach (var groupId in groupsIds)
                {
                    _context.UserGroups.Add(new UserGroup()
                    {
                        GroupId = groupId,
                        UserId = userId
                    });
                }
                _context.SaveChanges();
            }
        }

        public void RefreshUserGroupsRoles(string userId)
        {
            var user = FindUserById(userId);
            var roles = user.Roles.Select(a => a.RoleId).ToList();
            if (user.Roles.Count > 0)
            {
                RemoveUserAllRoles(userId);
            }
            var actualGroupRoles = GetUserGroupRoles(userId);
            var allRoles = GetAllRoles();
            var rolesToAdd = allRoles.Where(a => actualGroupRoles.Any(b => b.RoleId == a.Id)).Select(c => c.Id);
            AddUserToRoles(userId, rolesToAdd);
        }

        public void AddUserToRoles(string userId, IEnumerable<string> rolesToAdd)
        {
            var user = FindUserById(userId);
            foreach (var role in rolesToAdd)
            {
                user.Roles.Add(new IdentityUserRole()
                {
                    UserId = userId,
                    RoleId = role
                });
            }
            _context.SaveChanges();
        }

        public IEnumerable<ApplicationUser> GetGroupUsers(string groupId)
        {
            var group = FindGroupById(groupId);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in group.Users)
            {
                var user = _context.Users.Find(groupUser.UserId);
                users.Add(user);
            }
            return users;
        }

        public void RemoveRolesUsersAndEntireGroup(string groupId)
        {
            var group = FindGroupById(groupId);
            group.Users.Clear();
            group.Roles.Clear();
            _context.Groups.Remove(group);
            _context.SaveChanges();
        }

        public IEnumerable<IdentityRole> GetGroupRoles(string groupId)
        {
            var group = FindGroupById(groupId);
            var roles = GetAllRoles();
            var groupRolesIds = roles.Where(a => group.Roles.Any(b => b.RoleId == a.Id));
            return groupRolesIds;

        }

        public List<string> GetRolesIdsByNames(string[] roleNames)
        {
            var roles = _context.Roles.Where(a => roleNames.Any(b => b == a.Name)).ToList();
            var rolesIds = roles.Select(a => a.Id).ToList();
            return rolesIds;
        }

        public IEnumerable<Group> GetAllGroups()
        {
            var groups = _context.Groups.AsEnumerable();
            return groups;
        }

        public List<string> GetGroupRolesNames(string id)
        {
            var list = _context.GroupRoles.Where(a => a.GroupId == id).Select(a => a.Role.Name).ToList();
            return list;
        }

        public List<string> GetGroupUsersName(string id)
        {
            var list = _context.UserGroups.Where(a => a.GroupId == id).Select(a => a.User.UserName).ToList();
            return list;
        }

        public IEnumerable<ApplicationUser> GetAllUsers()
        {
            var users = _context.Users.AsEnumerable();
            return users;
        }

        public GroupIdAndNameDto GetGroupNameAndIdDto(string id)
        {
            var model = _context.Groups.Where(a => a.Id == id).Select(a => new GroupIdAndNameDto()
            {
                Id = a.Id,
                GroupName = a.GroupName
            }).FirstOrDefault();
            return model;
        }

        public void RemoveAllUsersFromGroup(Group group)
        {
            var groupEntity = FindGroupById(group.Id);
            var users = groupEntity.Users.ToList();
            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    groupEntity.Users.Remove(user);
                }
                _context.SaveChanges();
            }
        }

        public void AddUsersToGroup(Group group, string[] SelectedUsers)
        {
            if (SelectedUsers != null)
            {
                foreach (var user in SelectedUsers)
                {
                    group.Users.Add(new UserGroup
                    {
                        GroupId = group.Id,
                        UserId = user
                    });
                }
                _context.SaveChanges();
            }
        }

        public ApplicationUser GetUserById(string id)
        {
            var user = _context.Users.Single(a => a.Id == id);
            return user;
        }

        public void DeleteUser(string id)
        {
            var user = GetUserById(id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public void UpdateUser(EditUserDto model)
        {
            var user = FindUserById(model.Id);
            user.UserName = model.Username;
            user.Email = model.Email;
            user.FullName = model.FullName;
            RemoveUserFromAllGroups(model.Id);
            var SelectedGroups = model.SelectedGroups ?? new string[] { };
            AddUserToGroups(user.Id, SelectedGroups);
            RefreshUserGroupsRoles(model.Id);
        }

        public void RemoveUserFromAllGroups(string id)
        {
            var groups = _context.UserGroups.Where(a => a.UserId == id).ToList();
            foreach (var group in groups)
            {
                _context.UserGroups.Remove(group);
            }
            _context.SaveChanges();
        }

        public bool CheckIfUsernameAlreadyExistInDb(string username)
        {
            var usernameAlreadyExist = _context.Users.Any(a => a.UserName.ToUpper() == username.ToUpper());
            return usernameAlreadyExist;
        }

        public bool CheckIfEmailAlreadyExistInDb(string email)
        {
            var emailAlreadyExist = _context.Users.Any(a => a.Email.ToUpper() == email.ToUpper());
            return emailAlreadyExist;
        }

        public bool CheckIfUsernameAlreadyExistInDbAndNotOwnedByCurrentUser(string username, string currentUserId)
        {
            var usernameAlreadyExist = _context.Users.Any(a => a.UserName.ToUpper() == username.ToUpper() && a.UserName != currentUserId);
            return usernameAlreadyExist;
        }

        public string GetCurrentUserName(string id)
        {
            var userName = FindUserById(id).UserName;
            return userName;
        }

        public List<IdentityRole> GetAllRoles()
        {
            var roles = _context.Roles.ToList();
            return roles;
        }

        public IEnumerable<GroupRole> GetUserGroupRoles(string userId)
        {
            var userGroups = GetUserGroups(userId);
            var userGroupsRoles = new List<GroupRole>();
            foreach (var group in userGroups)
            {
                userGroupsRoles.AddRange(group.Roles.ToArray());
            }
            return userGroupsRoles;
        }

        public void RemoveUserAllRoles(string userId)
        {
            var user = FindUserById(userId);
            user.Roles.Clear();
            _context.SaveChanges();
        }

        public ApplicationUser FindUserById(string userId)
        {
            var user = _context.Users.SingleOrDefault(a => a.Id == userId);
            return user;
        }



    }
}