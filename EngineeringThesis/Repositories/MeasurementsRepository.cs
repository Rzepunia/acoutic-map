﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Device.Location;
using System.Linq;
using System.Web;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Repositories
{
    public class MeasurementsRepository : IMeasurementsRepository
    {
        private readonly ApplicationDbContext _context;

        public MeasurementsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Measurement> GetTodaysMeasurements()
        {
            var list = _context.Measurements.Where(a => a.DateTimeStamp.Day == DateTime.Today.Day && a.DateTimeStamp.Month== DateTime.Today.Month && a.DateTimeStamp.Year == DateTime.Today.Year).ToList();
            return list;
        }

        public List<Measurement> GetAllMeasurements()
        {
            var list = _context.Measurements.ToList();
            return list;
        }

        public List<Measurement> GetPointMeasurementsFromDate(string pointId, DateTime dateBegin, DateTime dateEnd)
        {
            var list = _context.Measurements.Where(a => a.PointId == pointId && a.DateTimeStamp >= dateBegin && a.DateTimeStamp <= dateEnd).ToList();
            return list;
        }

        public string GetClosestPointIdByCoordinates(float latitude, float longitude)
        {
            var closest = _context.Points.Min(a => (Math.Abs(a.Latitude - latitude) + Math.Abs(a.Longitude - longitude)));
            var point =
                _context.Points.First(a => (Math.Abs(a.Latitude - latitude) + Math.Abs(a.Longitude - longitude)) == closest);
            return point.Id;
        }

        public void AddNewMsrt(Measurement model)
        {
            _context.Measurements.Add(model);
            _context.SaveChanges();
        }
    }
}