﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EngineeringThesis.Entities;

namespace EngineeringThesis.Repositories
{
    public interface IMeasurementsRepository
    {
        List<Measurement> GetTodaysMeasurements();
        List<Measurement> GetAllMeasurements();
        List<Measurement> GetPointMeasurementsFromDate(string pointId, DateTime dateBegin, DateTime dateEnd);
        string GetClosestPointIdByCoordinates(float latitude, float longitude);
        void AddNewMsrt(Measurement model);
    }
}
