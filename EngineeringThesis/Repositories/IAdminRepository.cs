﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EngineeringThesis.Repositories
{
    public interface IAdminRepository
    {
        string CreateGroupAndReturnId(Group group);
        void DeleteGroup(Group group);
        Group FindGroupById(string id);
        void UpdateGroup(Group group);
        void ClearGroupRoles(string groupId);
        void AddRoleToGroup(string groupId, string role);
        void ResetRolesForGroupUsers(string groupId);
        void ClearGroupsForUser(string userId, List<string> groupIds);
        List<Group> GetUserGroups(string userId);
        List<string> GetUserGroupsIds(string userId);
        void AddUserToGroups(string userId, string[] groupsIds);
        void RefreshUserGroupsRoles(string userId);
        ApplicationUser FindUserById(string userId);
        IEnumerable<GroupRole> GetUserGroupRoles(string userId);
        List<IdentityRole> GetAllRoles();
        void AddUserToRoles(string userId, IEnumerable<string> rolesToAdd);
        IEnumerable<ApplicationUser> GetGroupUsers(string groupId);
        void RemoveRolesUsersAndEntireGroup(string groupId);
        IEnumerable<IdentityRole> GetGroupRoles(string groupId);
        List<string> GetRolesIdsByNames(string[] roleNames);
        IEnumerable<Group> GetAllGroups();
        List<string> GetGroupRolesNames(string id);
        List<string> GetGroupUsersName(string id);
        IEnumerable<ApplicationUser> GetAllUsers();
        GroupIdAndNameDto GetGroupNameAndIdDto(string id);
        void RemoveAllUsersFromGroup(Group group);
        void AddUsersToGroup(Group group, string[] SelectedUsers);
        ApplicationUser GetUserById(string id);
        void DeleteUser(string id);
        void UpdateUser(EditUserDto model);
        void RemoveUserFromAllGroups(string id);
        bool CheckIfUsernameAlreadyExistInDb(string username);
        bool CheckIfEmailAlreadyExistInDb(string email);
        bool CheckIfUsernameAlreadyExistInDbAndNotOwnedByCurrentUser(string username, string currentUserId);
        string GetCurrentUserName(string id);
    }
}
