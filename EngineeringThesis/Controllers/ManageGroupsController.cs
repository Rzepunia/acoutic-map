﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EngineeringThesis.Dtos;
using EngineeringThesis.Services;
using X.PagedList;


namespace EngineeringThesis.Controllers
{
    //[Authorize(Roles = "Admin Panel")]
    public class ManageGroupsController : Controller
    {
        private IAdminService _groupService;
        private readonly int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["manageGroupsIndexPageSize"]);

        public ManageGroupsController(IAdminService groupService)
        {
            _groupService = groupService;
        }

        [HttpGet]
        public ActionResult Index(int page=1)
        {
            var groups = _groupService.GetListOfGroupsWithRoles();
            var pagedList = groups.ToPagedList(page, pageSize);
            return View(pagedList);
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            var group = _groupService.GetGroupDetails(id);
            return View(group);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            _groupService.DeleteGroup(id);
            TempData["message"] = "You succesfully removed group!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _groupService.GetCreateModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateGroupDto group)
        {
            if (ModelState.IsValid)
            {
                _groupService.CreateGroup(group);
                TempData["message"] = "You succesfully created new group!";
                return RedirectToAction("Index");
            }
            var model = _groupService.GetCreateModel();
            model.GroupName = group.GroupName;
            model.GroupDescription = group.GroupDescription;
            return View(model);

        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var model = _groupService.GetGroupToEdit(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditGroupDto group)
        {
            if (ModelState.IsValid)
            {
                _groupService.UpdateGroup(group);
                TempData["message"] = "You succesfully edited group!";
                return RedirectToAction("Index");
            }
            var model = _groupService.GetGroupToEdit(group.Id);
            model.GroupName = group.GroupName;
            model.GroupDescription = group.GroupDescription;
            return View(model);

        }

        [HttpGet]
        public ActionResult AssignUsers(string id)
        {
            var model = _groupService.GetAssignUserViewModel(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignUsers(AssignUserToGroupDto group)
        {
            _groupService.AssignUsersToGroup(group);
            TempData["message"] = "You succesfully assigned users to group!";
            return RedirectToAction("Index");
        }

    }
}