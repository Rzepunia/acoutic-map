﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EngineeringThesis.Models;
using EngineeringThesis.Services;

namespace EngineeringThesis.Controllers
{
    public class MeasurementsController : Controller
    {
        private readonly IMeasurementsService _measurementsService;
        private readonly IPointAndAreaService _pointAndAreaService;

        public MeasurementsController(IMeasurementsService measurementsService, IPointAndAreaService pointAndAreaService)
        {
            _measurementsService = measurementsService;
            _pointAndAreaService = pointAndAreaService;
        }

        //[Authorize(Roles = "Admin Panel,Managing Points and Areas,Reports")]
        [HttpGet]
        public ActionResult TodaysReport()
        {
            var model = _measurementsService.GetTodaysReportDropdown();
            return View(model);
        }

        [HttpGet]
        public ActionResult LoadTodayData()
        {
            var data = _measurementsService.GetTodaysRaportData();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        //[Authorize(Roles = "Admin Panel,Managing Points and Areas,Reports")]
        [HttpGet]
        public ActionResult Report()
        {
            var model = _measurementsService.GetReportDropdown();
            return View(model);
        }

        [HttpGet]
        public ActionResult LoadRaportData()
        {
            var data = _measurementsService.GetRaportData();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        //[Authorize(Roles = "Admin Panel,Managing Points and Areas,Charts")]
        [HttpGet]
        public ActionResult Charts()
        {
            var data = _pointAndAreaService.GetAreasDropdown();
            return View(data);
        }

        [HttpGet]
        public ActionResult GetPointsByAreaId(string areaId)
        {
            var list = _pointAndAreaService.GetPointsSelectListByAreaId(areaId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetChartData(string pointId, string date)
        {
            var model = _measurementsService.GetChartData(pointId, date);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[Authorize(Roles = "Admin Panel,Managing Points and Areas,Map")]
        [HttpGet]
        public ActionResult Map()
        {
            var model = _measurementsService.GetMapMarkers();
            return View(model);
        }

        //Method to add measruement via special device, it sends measurement values by GPRS to turn on this method
        //adres/Measurements/NewMsrt?code=xx&lon=01131.0000E&lat=4807.0380N&msrt=0000&time=20160101132213
        [HttpGet]
        public void NewMsrt(string code, string lat, string lon, string msrt, string time)
        {
            if (code == "xx")
            {
                _measurementsService.AddNewMsrt(lat, lon, msrt, time);
            }
        }
    }
}