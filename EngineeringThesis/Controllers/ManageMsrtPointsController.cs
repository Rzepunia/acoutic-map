﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace EngineeringThesis.Controllers
{
    //[Authorize(Roles = "Admin Panel,Managing Points and Areas")]
    public class ManageMsrtPointsController : Controller
    {
        private readonly IPointAndAreaService _pointAndAreaService;
        private readonly int _pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["manageMsrtPointsIndexPageSize"]);

        public ManageMsrtPointsController(IPointAndAreaService pointAndAreaService)
        {
            _pointAndAreaService = pointAndAreaService;
        }

        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            var model = _pointAndAreaService.GetManagePointsViewModels();
            var viewModel = model.ToPagedList(page, _pageSize);
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            var model = _pointAndAreaService.GetPointDetails(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            _pointAndAreaService.DeletePoint(id);
            TempData["message"] = "You succesfully removed point!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _pointAndAreaService.GetCreatePointViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CreatePointDto model)
        {
            if (ModelState.IsValid)
            {
                _pointAndAreaService.CreateNewPoint(model);
                TempData["message"] = "You succesfully created new point!";
                return RedirectToAction("Index");
            }
            var invalidModel = _pointAndAreaService.GetCreatePointViewModelFromInvalidModel(model);
            return View(invalidModel);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var model = _pointAndAreaService.GetEditPointViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditPointDto model)
        {
            if (ModelState.IsValid)
            {
                _pointAndAreaService.UpdatePoint(model);
                TempData["message"] = "You succesfully edited point!";
                return RedirectToAction("Index");
            }
            var invalidModel = _pointAndAreaService.GetEditPointViewModelFromInvalidModel(model);
            return View(invalidModel);
        }
    }
}