﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EngineeringThesis.Resources;
using EngineeringThesis.Services;
using X.PagedList;

namespace EngineeringThesis.Controllers
{
    //[Authorize(Roles = "Admin Panel")]
    public class ManageUsersController : Controller
    {
        private readonly IAdminService _adminService;
        private readonly int usersOnSite = Convert.ToInt32(ConfigurationManager.AppSettings["manageUsersIndexPageSize"]);

        public ManageUsersController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            var list = _adminService.GetUsersListToShow();
            var model = list.ToPagedList(page, usersOnSite);
            return View(model);
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            var model = _adminService.GetUserDetails(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            _adminService.DeleteUser(id);
            TempData["message"] = "You succesfully removed user account!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _adminService.GetRegisterViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RegisterUserByAdminDto model)
        {
            var usernameAlreadyExist = _adminService.CheckIfUsernameAlreadyExist(model.Username);
            if (usernameAlreadyExist)
            {
                ModelState.AddModelError("Username", BackEndResources.ValidationMessages_UsernameAlreadyExist);
            }
            var emailAlreadyExist = _adminService.CheckIfEmailAlreadyExist(model.Email);
            if (emailAlreadyExist)
            {
                ModelState.AddModelError("Email", BackEndResources.ValidationMessages_EmailAlreadyExist);
            }
            if (ModelState.IsValid)
            {
                _adminService.CreateNewUser(model);
                TempData["message"] = "You succesfully created new account!";
                return RedirectToAction("Index");
            }
            var viewModel = _adminService.GetRegisterViewModelFromInvalidModel(model);
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var model = _adminService.GetEditUserViewModel(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserDto model)
        {
            var usernameAlreadyExist = _adminService.CheckIfUsernameAlreadyExistAndNotOwnedByCurrentUser(model.Id, model.Username);
            if (usernameAlreadyExist)
            {
                ModelState.AddModelError("Username", BackEndResources.ValidationMessages_UsernameAlreadyExist);
            }
            if (ModelState.IsValid)
            {
                _adminService.UpdateUser(model);
                TempData["message"] = "Your succesfully edited user account!";
                return RedirectToAction("Index");
            }
            var viewModel = _adminService.GetEditUserViewModelFromInvalidModel(model);
            return View(viewModel);
        }
    }
}