﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace EngineeringThesis.Controllers
{
    //[Authorize(Roles = "Admin Panel,Managing Points and Areas")]
    public class ManageMsrtAreasController : Controller
    {
        private readonly IPointAndAreaService _pointAndAreaService;

        private readonly int _pageSize =
            Convert.ToInt32(ConfigurationManager.AppSettings["manageMsrtAreasIndexPageSize"]);

        public ManageMsrtAreasController(IPointAndAreaService pointAndAreaService)
        {
            _pointAndAreaService = pointAndAreaService;
        }

        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            var model = _pointAndAreaService.GetManageAreassViewModels();
            var viewModel = model.ToPagedList(page, _pageSize);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            _pointAndAreaService.DeleteArea(id);
            TempData["message"] = "You succesfully removed area!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateAreaDto model)
        {
            if (ModelState.IsValid)
            {
                _pointAndAreaService.CreateNewArea(model);
                TempData["message"] = "You succesfully created new area!";
                return RedirectToAction("Index");
            }
            var invalidModel = _pointAndAreaService.GetCreateAreaViewModelFromInvalidModel(model);
            return View(invalidModel);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var model = _pointAndAreaService.GetEditAreaViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditAreaDto model)
        {
            if (ModelState.IsValid)
            {
                _pointAndAreaService.UpdateArea(model);
                TempData["message"] = "You succesfully edited area!";
                return RedirectToAction("Index");
            }
            var invalidModel = _pointAndAreaService.GetEditAreaViewModelFromInvalidModel(model);
            return View(invalidModel);
        }

        [HttpGet]
        public ActionResult AssignPoints(string id)
        {
            var model = _pointAndAreaService.GetAssignPointsToAreaViewModel(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignPoints(AssignPointsToAreaDto model)
        {
            _pointAndAreaService.AssignPointsToArea(model);
            TempData["message"] = "You succesfully assigned points to area!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            var model = _pointAndAreaService.GetAreaDetails(id);
            return View(model);
        }
    }
}