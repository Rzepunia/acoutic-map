﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Entities
{
    public class Point
    {
        public Point()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public string Localization { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
        public virtual ICollection<Measurement> Measrments { get; set; }
    }
}