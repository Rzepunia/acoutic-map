﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EngineeringThesis.Entities
{
    public class Measurement
    {
        [Key]
        public string Id { get; set; }
        [ForeignKey("Point")]
        public string PointId { get; set; }
        public DateTime DateTime { get; set; }
        public int Value { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public virtual Point Point { get; set; }
    }
}