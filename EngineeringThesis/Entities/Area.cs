﻿using System;
using System.Collections.Generic;

namespace EngineeringThesis.Entities
{
    public class Area
    {
        public Area()
        {
            Id = Guid.NewGuid().ToString();
            Points = new List<Point>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Point> Points { get; set; }
    }
}