﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EngineeringThesis.Entities
{
    public class Group
    {
        public Group()
        {
            Id = Guid.NewGuid().ToString();
            Roles = new List<GroupRole>();
            Users = new List<UserGroup>();
        }

        [Key]
        public string Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public virtual ICollection<UserGroup> Users { get; set; }
        public virtual ICollection<GroupRole> Roles { get; set; }
    }
    
    public class UserGroup
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("User")]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Group")]
        public string GroupId { get; set; }

        public virtual Group Group { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class GroupRole
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("Group")]
        public string GroupId { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Role")]
        public string RoleId { get; set; }

        public virtual Group Group { get; set; }
        public virtual IdentityRole Role { get; set; }
    }
}