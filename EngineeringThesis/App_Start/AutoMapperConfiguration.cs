﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;
using EngineeringThesis.Services;

namespace EngineeringThesis.App_Start
{
    public static class AutoMapperConfiguration
    {
        public static IMapper ConfigureMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CreateGroupDto, Group>();
                cfg.CreateMap<EditGroupDto, Group>();
                cfg.CreateMap<ApplicationUser, UserDetailsViewModel>();
                cfg.CreateMap<ApplicationUser, EditUserViewModel>();
                cfg.CreateMap<RegisterUserByAdminDto, ApplicationUser>();
                cfg.CreateMap<ApplicationUser, ManageUsersViewModel>();
                cfg.CreateMap<GroupIdAndNameDto, AssignUserToGroupViewModel>();
                cfg.CreateMap<Group, EditGroupViewModel>();
                cfg.CreateMap<Group, GroupDetailsViewModel>();
                cfg.CreateMap<Group, GroupViewModel>();
                cfg.CreateMap<Point, ManagePointsViewModel>()
                .ForMember(p => p.Latitude, opt => opt.MapFrom(p => p.Latitude.ToString(CultureInfo.InvariantCulture)))
                .ForMember(p => p.Longitude, opt => opt.MapFrom(p => p.Longitude.ToString(CultureInfo.InvariantCulture)));
                cfg.CreateMap<Point, PointDetailsViewModel>()
                .ForMember(p => p.Latitude, opt => opt.MapFrom(p => p.Latitude.ToString(CultureInfo.InvariantCulture)))
                .ForMember(p => p.Longitude, opt => opt.MapFrom(p => p.Longitude.ToString(CultureInfo.InvariantCulture)));
                cfg.CreateMap<CreatePointDto, CreatePointViewModel>();
                cfg.CreateMap<CreatePointDto, Point>()
                .ForMember(dto => dto.Latitude, opt => opt.MapFrom(dto => float.Parse(dto.Latitude, CultureInfo.InvariantCulture.NumberFormat)))
                .ForMember(dto => dto.Longitude, opt => opt.MapFrom(dto => float.Parse(dto.Longitude, CultureInfo.InvariantCulture.NumberFormat)));
                cfg.CreateMap<Point, EditPointViewModel>()
                .ForMember(p => p.Latitude, opt => opt.MapFrom(p => p.Latitude.ToString(CultureInfo.InvariantCulture)))
                .ForMember(p => p.Longitude, opt => opt.MapFrom(p => p.Longitude.ToString(CultureInfo.InvariantCulture)));
                cfg.CreateMap<EditPointDto, EditPointViewModel>();
                cfg.CreateMap<EditPointDto, Point>()
                .ForMember(dto => dto.Latitude, opt => opt.MapFrom(dto => float.Parse(dto.Latitude, CultureInfo.InvariantCulture.NumberFormat)))
                .ForMember(dto => dto.Longitude, opt => opt.MapFrom(dto => float.Parse(dto.Longitude, CultureInfo.InvariantCulture.NumberFormat)));
                cfg.CreateMap<Area, AreaViewModel>();
                cfg.CreateMap<Point, PointViewModel>();
                cfg.CreateMap<Area, ManageAreasViewModel>();
                cfg.CreateMap<CreateAreaDto, Area>();
                cfg.CreateMap<CreateAreaDto, CreateAreaViewModel>();
                cfg.CreateMap<Area, EditAreaViewModel>();
                cfg.CreateMap<EditAreaDto, Area>();
                cfg.CreateMap<EditAreaDto, EditAreaViewModel>();
                cfg.CreateMap<Measurement, MeasurementViewModel>()
                .ForMember(dest => dest.PointName, opt => opt.MapFrom(src => src.Point.Name));
                cfg.CreateMap<Measurement, MapMarkersViewModel>()
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Point.Name))
                    .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Point.Latitude))
                    .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Point.Longitude));
                cfg.CreateMap<Measurement, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Point.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Point.Name));
                cfg.CreateMap<Area, AreaDetailsViewModel>();
                cfg.CreateMap<Area, SelectListItem>()
                   .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                   .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));
                cfg.CreateMap<Point, SelectListItem>()
                 .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));
                cfg.CreateMap<RegisterUserByAdminDto, RegisterUserByAdminViewModel>();
            });
            var mapper = configuration.CreateMapper();
            return mapper;
        }
    }
}