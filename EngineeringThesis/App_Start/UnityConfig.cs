using System;
using System.Data.Entity;
using System.Web;
using AutoMapper;
using EngineeringThesis.Controllers;
using EngineeringThesis.Entities;
using EngineeringThesis.Infrastructure;
using EngineeringThesis.Repositories;
using EngineeringThesis.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace EngineeringThesis.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IMapper>(new InjectionFactory(c => AutoMapperConfiguration.ConfigureMapper()));
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<IMappingInfrastructure, MappingInfrastructure>();
            container.RegisterType<IAdminRepository, AdminRepository>();
            container.RegisterType<ManageController>(new InjectionConstructor()); ;
            container.RegisterType<IAdminService, AdminService>();
            container.RegisterType<ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IPointAndAreaService, PointAndAreaService>();
            container.RegisterType<IPointAndAreaRepository, PointAndAreaRepository>();
            container.RegisterType<IMeasurementsService, MeasurementsService>();
            container.RegisterType<IMeasurementsRepository, MeasurementsRepository>();
            container.RegisterType<ICalculatingCoordinatesInfrastructure, CalculatingCoordinatesInfrastructure>();
        }
    }
}
