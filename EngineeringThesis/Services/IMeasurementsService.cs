﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using EngineeringThesis.Models;

namespace EngineeringThesis.Services
{
    public interface IMeasurementsService
    {
        List<MeasurementViewModel> GetTodaysRaportData();
        List<SelectListItem> GetTodaysReportDropdown();
        List<MeasurementViewModel> GetRaportData();
        List<SelectListItem> GetReportDropdown();
        ChartDataViewModel GetChartData(string pointId, string date);
        List<MapMarkersViewModel> GetMapMarkers();
        void AddNewMsrt(string lat, string lon, string msrt, string time);
    }
}
