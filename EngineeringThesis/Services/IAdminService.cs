﻿using System.Collections.Generic;
using System.Web.Mvc;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;

namespace EngineeringThesis.Services
{
    public interface IAdminService
    {
        void CreateGroup(CreateGroupDto group);
        void SetGroupRoles(string groupId, string[] roleNames);
        void SetUserGroups(string userId, string[] groupsIds);
        void DeleteGroup(string groupId);
        void ClearUserGroups(string userId);
        void UpdateGroup(EditGroupDto group);
        IEnumerable<GroupViewModel> GetListOfGroupsWithRoles();
        GroupDetailsViewModel GetGroupDetails(string id);
        CreateGroupViewModel GetCreateModel();
        EditGroupViewModel GetGroupToEdit(string groupId);
        AssignUserToGroupViewModel GetAssignUserViewModel(string id);
        void AssignUsersToGroup(AssignUserToGroupDto group);
        List<ManageUsersViewModel> GetUsersListToShow();
        UserDetailsViewModel GetUserDetails(string id);
        void DeleteUser(string id);
        RegisterUserByAdminViewModel GetRegisterViewModel();
        void CreateNewUser(RegisterUserByAdminDto model);
        EditUserViewModel GetEditUserViewModel(string id);
        void UpdateUser(EditUserDto model);
        bool CheckIfUsernameAlreadyExist(string username);
        bool CheckIfEmailAlreadyExist(string email);
        RegisterUserByAdminViewModel GetRegisterViewModelFromInvalidModel(RegisterUserByAdminDto model);
        EditUserViewModel GetEditUserViewModelFromInvalidModel(EditUserDto model);
        bool CheckIfUsernameAlreadyExistAndNotOwnedByCurrentUser(string id, string username);
    }
}