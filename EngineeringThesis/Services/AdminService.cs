﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using EngineeringThesis.Controllers;
using EngineeringThesis.Dtos;
using EngineeringThesis.Repositories;
using EngineeringThesis.Entities;
using EngineeringThesis.Infrastructure;
using EngineeringThesis.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EngineeringThesis.Services
{
    public class AdminService : IAdminService
    {
        private IAdminRepository _adminRepository;
        private IMappingInfrastructure _mappingInfrastructure;
        private ApplicationUserManager _userManager;

        public AdminService(IAdminRepository adminRepository, IMappingInfrastructure mappingInfrastructure, ApplicationUserManager userManager)
        {
            _adminRepository = adminRepository;
            _mappingInfrastructure = mappingInfrastructure;
            _userManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public void CreateGroup(CreateGroupDto group)
        {
            var model = _mappingInfrastructure.MapGroupDtoToGroup(group);
            var id = _adminRepository.CreateGroupAndReturnId(model);
            SetGroupRoles(id, group.SelectedRoles);
        }

        public void SetGroupRoles(string groupId, string[] roleNames)
        {
            _adminRepository.ClearGroupRoles(groupId);
            if (roleNames != null)
            {
                foreach (var role in roleNames)
                {
                    _adminRepository.AddRoleToGroup(groupId, role);
                }
                _adminRepository.ResetRolesForGroupUsers(groupId);
            }
        }

        public void SetUserGroups(string userId, string[] groupsIds)
        {
            var currentGroupsIds = _adminRepository.GetUserGroupsIds(userId);
            _adminRepository.ClearGroupsForUser(userId, currentGroupsIds);
            _adminRepository.AddUserToGroups(userId, groupsIds);
            _adminRepository.RefreshUserGroupsRoles(userId);
        }

        public void DeleteGroup(string groupId)
        {
            var currentGroupMembers = _adminRepository.GetGroupUsers(groupId);
            _adminRepository.RemoveRolesUsersAndEntireGroup(groupId);
            foreach (var user in currentGroupMembers)
            {
                _adminRepository.RefreshUserGroupsRoles(user.Id);
            }
        }

        public void ClearUserGroups(string userId)
        {
            SetUserGroups(userId, null);
        }

        public IEnumerable<GroupViewModel> GetListOfGroupsWithRoles()
        {
            var groups = _adminRepository.GetAllGroups();
            var list = new List<GroupViewModel>();
            foreach (var group in groups)
            {
                var groupViewModel = _mappingInfrastructure.MapGroupAndRolesToGroupViewModel(group);
                list.Add(groupViewModel);
            }
            return list.AsEnumerable();

        }

        public GroupDetailsViewModel GetGroupDetails(string id)
        {
            var group = _adminRepository.FindGroupById(id);
            var groupDetails = _mappingInfrastructure.MapGroupToGroupDetailsViewModel(group);
            return groupDetails;
        }

        public CreateGroupViewModel GetCreateModel()
        {
            var roles = _adminRepository.GetAllRoles();
            var model = _mappingInfrastructure.MapRolesToCreateGroupViewModel(roles);
            return model;
        }

        public EditGroupViewModel GetGroupToEdit(string groupId)
        {
            var selectedRoles = _adminRepository.GetGroupRoles(groupId);
            var group = _adminRepository.FindGroupById(groupId);
            var allRoles = _adminRepository.GetAllRoles();
            var model = _mappingInfrastructure.MapGroupAndRolesToEditGroupViewModel(group, selectedRoles, allRoles);
            return model;
        }

        public AssignUserToGroupViewModel GetAssignUserViewModel(string id)
        {
            var group = _adminRepository.GetGroupNameAndIdDto(id);
            var actualUsers = _adminRepository.GetGroupUsers(id);
            var allUsers = _adminRepository.GetAllUsers();
            var model = _mappingInfrastructure.MapUsersAndGroupIdToAssignUserToGroupViewModel(group, actualUsers, allUsers);
            return model;

        }

        public void AssignUsersToGroup(AssignUserToGroupDto group)
        {
            var groupEntity = _adminRepository.FindGroupById(group.Id);
            var formerAndPresentGroupUsers = groupEntity.Users.Select(a => a.UserId).ToList();
            if (group.SelectedUsers != null)
            {
                foreach (var user in group.SelectedUsers)
                {
                    if (!formerAndPresentGroupUsers.Contains(user))
                    {
                        formerAndPresentGroupUsers.Add(user);
                    }
                }
            }
            _adminRepository.RemoveAllUsersFromGroup(groupEntity);
            _adminRepository.AddUsersToGroup(groupEntity, group.SelectedUsers);
            foreach (var user in formerAndPresentGroupUsers)
            {
                _adminRepository.RefreshUserGroupsRoles(user);
            }

        }

        public List<ManageUsersViewModel> GetUsersListToShow()
        {
            var users = _adminRepository.GetAllUsers();
            var list = new List<ManageUsersViewModel>();
            foreach (var user in users)
            {
                var groups = _adminRepository.GetUserGroups(user.Id).Select(a => a.GroupName).ToList();
                var element = _mappingInfrastructure.MapUserAndGroupsToManageUsersViewModel(user, groups);
                list.Add(element);
            }
            return list;
        }

        public UserDetailsViewModel GetUserDetails(string id)
        {
            var user = _adminRepository.GetUserById(id);
            var userGroups = _adminRepository.GetUserGroups(id).Select(a => a.GroupName).ToList();
            var model = _mappingInfrastructure.MapUserAndGroupsToDetailsViewModel(user, userGroups);
            return model;
        }

        public void DeleteUser(string id)
        {
            _adminRepository.DeleteUser(id);
        }

        public RegisterUserByAdminViewModel GetRegisterViewModel()
        {
            var allGroups = _adminRepository.GetAllGroups();
            var model = _mappingInfrastructure.MapAllGroupsToRegisterUserByAdminViewModel(allGroups);
            return model;
        }

        public void CreateNewUser(RegisterUserByAdminDto model)
        {
            var user = _mappingInfrastructure.MapRegisterUserByAdminDtoToApplicationUser(model);
            var addResult = UserManager.Create(user, model.Password);

            if (addResult.Succeeded)
            {
                if (model.SelectedGroups != null)
                {
                    _adminRepository.AddUserToGroups(user.Id, model.SelectedGroups);
                    _adminRepository.RefreshUserGroupsRoles(user.Id);
                }
            }
        }

        public EditUserViewModel GetEditUserViewModel(string id)
        {
            var user = UserManager.FindById(id);
            var allGroups = _adminRepository.GetAllGroups();
            var userGroups = _adminRepository.GetUserGroups(id);
            var model = _mappingInfrastructure.MapUserAndSelectedGroupsToEditUserViewModel(user, allGroups, userGroups);
            return model;
        }

        public void UpdateUser(EditUserDto model)
        {
            _adminRepository.UpdateUser(model);
        }

        public bool CheckIfUsernameAlreadyExist(string username)
        {
            var exist = _adminRepository.CheckIfUsernameAlreadyExistInDb(username);
            return exist;
        }

        public bool CheckIfEmailAlreadyExist(string email)
        {
            var exist = _adminRepository.CheckIfEmailAlreadyExistInDb(email);
            return exist;
        }

        public RegisterUserByAdminViewModel GetRegisterViewModelFromInvalidModel(RegisterUserByAdminDto model)
        {
            var allGroups = _adminRepository.GetAllGroups();
            var viewmodel = _mappingInfrastructure.MapAllGroupsAndRegisterUserByAdminDtoToRegisterUserByAdminViewModel(allGroups, model);
            return viewmodel;
        }

        public EditUserViewModel GetEditUserViewModelFromInvalidModel(EditUserDto model)
        {
            var user = UserManager.FindById(model.Id);
            var allGroups = _adminRepository.GetAllGroups();
            var userGroups = _adminRepository.GetUserGroups(model.Id);
            var viewmodel = _mappingInfrastructure.MapUserSelectedGroupsAndInvalidModelToEditUserViewModel(user, allGroups, userGroups,model);
            return viewmodel;
        }

        public bool CheckIfUsernameAlreadyExistAndNotOwnedByCurrentUser(string id, string username)
        {
            var currentUsername = _adminRepository.GetCurrentUserName(id);
            var exist = _adminRepository.CheckIfUsernameAlreadyExistInDbAndNotOwnedByCurrentUser(username, currentUsername);
            return exist;
        }

        public void UpdateGroup(EditGroupDto group)
        {
            var item = _mappingInfrastructure.MapEditGroupDtoToGroup(group);
            _adminRepository.UpdateGroup(item);
            SetGroupRoles(group.Id, group.SelectedRoles);
        }
    }
}

