﻿using System.Collections.Generic;
using System.Web.Mvc;
using EngineeringThesis.Entities;
using EngineeringThesis.Models;

namespace EngineeringThesis.Controllers
{
    public interface IPointAndAreaService
    {
        List<ManagePointsViewModel> GetManagePointsViewModels();
        void DeletePoint(string id);
        PointDetailsViewModel GetPointDetails(string id);
        CreatePointViewModel GetCreatePointViewModel();
        CreatePointViewModel GetCreatePointViewModelFromInvalidModel(CreatePointDto model);
        void CreateNewPoint(CreatePointDto model);
        EditPointViewModel GetEditPointViewModel(string pointId);
        EditPointViewModel GetEditPointViewModelFromInvalidModel(EditPointDto model);
        void UpdatePoint(EditPointDto model);
        List<ManageAreasViewModel> GetManageAreassViewModels();
        void DeleteArea(string id);
        void CreateNewArea(CreateAreaDto model);
        CreateAreaViewModel GetCreateAreaViewModelFromInvalidModel(CreateAreaDto model);
        EditAreaViewModel GetEditAreaViewModel(string areaId);
        void UpdateArea(EditAreaDto model);
        EditAreaViewModel GetEditAreaViewModelFromInvalidModel(EditAreaDto model);
        AssignPointsToAreaViewModel GetAssignPointsToAreaViewModel(string id);
        void AssignPointsToArea(AssignPointsToAreaDto model);
        List<SelectListItem> GetPointsSelectListByAreaId(string areaId);
        List<SelectListItem> GetAreasDropdown();
        AreaDetailsViewModel GetAreaDetails(string id);
    }
}