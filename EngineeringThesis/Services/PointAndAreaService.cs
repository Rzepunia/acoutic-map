﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EngineeringThesis.Controllers;
using EngineeringThesis.Entities;
using EngineeringThesis.Infrastructure;
using EngineeringThesis.Models;

namespace EngineeringThesis.Services
{
    public class PointAndAreaService : IPointAndAreaService
    {
        private readonly IPointAndAreaRepository _pointAndAreaRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;

        public PointAndAreaService(IPointAndAreaRepository pointAndAreaRepository, IMappingInfrastructure mappingInfrastructure)
        {
            _pointAndAreaRepository = pointAndAreaRepository;
            _mappingInfrastructure = mappingInfrastructure;
        }

        public List<ManagePointsViewModel> GetManagePointsViewModels()
        {
            var points = _pointAndAreaRepository.GetAllPoints();
            var viewmodel = _mappingInfrastructure.MapListOfPointsToListOfManagePointsViewModel(points);
            return viewmodel;
        }

        public void DeletePoint(string id)
        {
            _pointAndAreaRepository.DeletePoint(id);
        }

        public PointDetailsViewModel GetPointDetails(string id)
        {
            var point = _pointAndAreaRepository.FindPointById(id);
            var pointViewModel = _mappingInfrastructure.MapPointToPointDetailsViewModel(point);
            return pointViewModel;
        }

        public CreatePointViewModel GetCreatePointViewModel()
        {
            var model = new CreatePointViewModel("17.058171000000016", "51.109454");
            return model;
        }

        public CreatePointViewModel GetCreatePointViewModelFromInvalidModel(CreatePointDto model)
        {
            var viewModel = _mappingInfrastructure.MapCreatePointDtoToCreatePointViewModel(model);
            return viewModel;
        }

        public void CreateNewPoint(CreatePointDto model)
        {
            var entity = _mappingInfrastructure.MapCreatePointDtoToPointEntity(model);
            _pointAndAreaRepository.AddNewPoint(entity);
        }

        public EditPointViewModel GetEditPointViewModel(string pointId)
        {
            var entity = _pointAndAreaRepository.GetPointById(pointId);
            var viewModel = _mappingInfrastructure.MapPointToEditPointViewModel(entity);
            return viewModel;
        }

        public EditPointViewModel GetEditPointViewModelFromInvalidModel(EditPointDto model)
        {
            var viewModel = _mappingInfrastructure.MapEditPointDtoToCreatePointViewModel(model);
            return viewModel;
        }

        public void UpdatePoint(EditPointDto model)
        {
            var entity = _mappingInfrastructure.MapEditPointDtoToPointEntity(model);
            _pointAndAreaRepository.UpdatePoint(entity);
        }

        public List<ManageAreasViewModel> GetManageAreassViewModels()
        {
            var areas = _pointAndAreaRepository.GetAllAreas();
            var viewModel = _mappingInfrastructure.MapListOfAreasToListOfManageAreasViewModel(areas);
            return viewModel;
        }

        public void DeleteArea(string id)
        {
            _pointAndAreaRepository.DeleteArea(id);
        }

        public void CreateNewArea(CreateAreaDto model)
        {
            var entity = _mappingInfrastructure.MapCreateAreaDtoToAreaEntity(model);
            _pointAndAreaRepository.AddNewArea(entity);
        }

        public CreateAreaViewModel GetCreateAreaViewModelFromInvalidModel(CreateAreaDto model)
        {
            var viewModel = _mappingInfrastructure.MapCreateAreaDtoToCreateAreaViewModel(model);
            return viewModel;
        }

        public EditAreaViewModel GetEditAreaViewModel(string areaId)
        {
            var entity = _pointAndAreaRepository.GetAreaById(areaId);
            var viewModel = _mappingInfrastructure.MapAreaToEditAreaViewModel(entity);
            return viewModel;
        }

        public void UpdateArea(EditAreaDto model)
        {
            var entity = _mappingInfrastructure.MapEditAreaDtoToAreaEntity(model);
            _pointAndAreaRepository.UpdateArea(entity);
        }

        public EditAreaViewModel GetEditAreaViewModelFromInvalidModel(EditAreaDto model)
        {
            var viewModel = _mappingInfrastructure.MapEditAreaDtoToCreateAreaViewModel(model);
            return viewModel;
        }

        public AssignPointsToAreaViewModel GetAssignPointsToAreaViewModel(string id)
        {
            var area = _pointAndAreaRepository.GetAreaById(id);
            var allPoints = _pointAndAreaRepository.GetAllPoints();
            var model = _mappingInfrastructure.MapAreaToAssignPointsToAreaViewModel(area, allPoints);
            return model;
        }

        public void AssignPointsToArea(AssignPointsToAreaDto model)
        {
            var pointsToAssign = model.SelectedPoints ?? new string[] { };
            _pointAndAreaRepository.AssignPointsToArea(model.Id, pointsToAssign);
        }

        public List<SelectListItem> GetPointsSelectListByAreaId(string areaId)
        {
            List<Point> points;
            if (areaId == "0")
            {
                points = _pointAndAreaRepository.GetAllPoints();
            }
            else
            {
                points = _pointAndAreaRepository.GetAreaById(areaId).Points.ToList();
            }
            if (points != null)
            {
                var list = _mappingInfrastructure.MapListOfPointsToListOfSelectListItem(points);
                return list;
            }
            return new List<SelectListItem>();
        }

        public List<SelectListItem> GetAreasDropdown()
        {
            var areas = _pointAndAreaRepository.GetAllAreas();
            var list = _mappingInfrastructure.MapListOfAreasToListOfSelectListItem(areas);
            list.Add(new SelectListItem()
            {
                Text = "All areas",
                Value = 0.ToString(),
                Selected = true
            });
            return list;
        }

        public AreaDetailsViewModel GetAreaDetails(string id)
        {
            var point = _pointAndAreaRepository.FindAreaById(id);
            var pointViewModel = _mappingInfrastructure.MapAreaToAreaDetailsViewModel(point);
            return pointViewModel;
        }
    }
}