﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EngineeringThesis.Entities;
using EngineeringThesis.Infrastructure;
using EngineeringThesis.Models;
using EngineeringThesis.Repositories;

namespace EngineeringThesis.Services
{
    public class MeasurementsService : IMeasurementsService
    {
        private readonly IMeasurementsRepository _measurementsRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;
        private readonly IPointAndAreaRepository _pointAndAreaRepository;
        private readonly ICalculatingCoordinatesInfrastructure _calculatingCoordinatesInfrastructure;

        public MeasurementsService(IMeasurementsRepository measurementsRepository, IMappingInfrastructure mappingInfrastructure,
            IPointAndAreaRepository pointAndAreaRepository, ICalculatingCoordinatesInfrastructure calculatingCoordinatesInfrastructure)
        {
            _measurementsRepository = measurementsRepository;
            _mappingInfrastructure = mappingInfrastructure;
            _pointAndAreaRepository = pointAndAreaRepository;
            _calculatingCoordinatesInfrastructure = calculatingCoordinatesInfrastructure;
        }

        public List<MeasurementViewModel> GetTodaysRaportData()
        {
            var listOfEntities = _measurementsRepository.GetTodaysMeasurements();
            var list = _mappingInfrastructure.MapListOfMeasurementsToListOfMeasurementsViewModel(listOfEntities);
            return list;
        }

        public List<SelectListItem> GetTodaysReportDropdown()
        {
            var points = _measurementsRepository.GetTodaysMeasurements();
            var uniqueList = points.GroupBy(a => a.Point.Name).Select(b => b.First()).ToList();
            var list = _mappingInfrastructure.MapListOfMeasurementsToListOfSelectListItem(uniqueList);
            list.Add(new SelectListItem()
            {
                Text = "All points",
                Value = "",
                Selected = true
            });
            return list;
        }

        public List<MeasurementViewModel> GetRaportData()
        {
            var listOfEntities = _measurementsRepository.GetAllMeasurements();
            var list = _mappingInfrastructure.MapListOfMeasurementsToListOfMeasurementsViewModel(listOfEntities);
            return list;
        }

        public List<SelectListItem> GetReportDropdown()
        {
            var points = _measurementsRepository.GetAllMeasurements();
            var uniqueList = points.GroupBy(a => a.Point.Name).Select(b => b.First()).ToList();
            var list = _mappingInfrastructure.MapListOfMeasurementsToListOfSelectListItem(uniqueList);
            list.Add(new SelectListItem()
            {
                Text = "All points",
                Value = "",
                Selected = true
            });
            return list;
        }

        public ChartDataViewModel GetChartData(string pointId, string date)
        {
            var date1 = date.Substring(0, 10);
            var date2 = date.Substring(14);
            var dateBegin = DateTime.ParseExact(date1, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            var dateEnd = DateTime.ParseExact(date2, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            var measurements = _measurementsRepository.GetPointMeasurementsFromDate(pointId, dateBegin, dateEnd);
            var viewModel = _mappingInfrastructure.MapListOfMeasurementsToChartDataViewModel(measurements);
            return viewModel;
        }

        public List<MapMarkersViewModel> GetMapMarkers()
        {
            var data = _measurementsRepository.GetAllMeasurements();
            var sortedData = data.OrderByDescending(a => a.DateTimeStamp);
            var listOfMsrts = new List<Measurement>();
            var uniquePoints = data.Select(a => a.PointId).Distinct();
            foreach (var item in uniquePoints)
            {
                listOfMsrts.Add(sortedData.First(a => a.PointId == item));
            }
            var list = _mappingInfrastructure.MapListOfMeasurementsToListOfMapMarkersViewModel(listOfMsrts);
            return list;
        }

        public void AddNewMsrt(string lat, string lon, string msrt, string time)
        {
            var latitude = _calculatingCoordinatesInfrastructure.CalculateLatitude(lat);
            var longitude = _calculatingCoordinatesInfrastructure.CalculateLongitude(lon); ;
            var pointId = _measurementsRepository.GetClosestPointIdByCoordinates(latitude, longitude);
            var parsedTime = DateTime.ParseExact(time, "yyyyMMddHHmmss", CultureInfo.CurrentCulture);
            var measruementValue = Int32.Parse(msrt);
            var entityModel = _mappingInfrastructure.MapPointIdTimeAndMsrtValueToMeasurementEntity(pointId,
                measruementValue, parsedTime);
            _measurementsRepository.AddNewMsrt(entityModel);
        }
    }
}
